$().ready(function() {

    var ws = new WebSocket('ws://chatty001.herokuapp.com/ws');
    var json = {
        'name': 'ユーザー',
        'x': 0,
        'y': 0
    };

    $('#my-cursor').draggable({
        containment: 'parent',
        drag: function(event, ui) {
            json.x = ui.position.left;
            json.y = ui.position.top;
            ws.send(JSON.stringify(json));
            console.log(json);
        }
    });

});
