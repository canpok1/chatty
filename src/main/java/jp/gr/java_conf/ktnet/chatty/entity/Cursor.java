package jp.gr.java_conf.ktnet.chatty.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * カーソル情報を保持するクラスです.
 * @author tanabe
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cursor {

  /** カーソルの名前. */
  private String name;
  
  /** カーソルのX座標. */
  private int x;
  
  /** カーソルのY座標. */
  private int y;
  
  
  /**
   * JSON文字列からオブジェクトを生成します.
   * @param json JSON文字列.
   * @return オブジェクト.
   */
  public static Cursor makeObject(@NonNull String json) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(Feature.ALLOW_SINGLE_QUOTES, true);
      return mapper.readValue(json, Cursor.class);
    } catch (Exception e) {
      throw new IllegalArgumentException(e);
    }
  }
  
  /**
   * オブジェクトからJSON文字列を生成します.
   * @param object オブジェクト.
   * @return JSON文字列.
   */
  public static String makeJson(@NonNull Cursor object) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      return mapper.writeValueAsString(object);
    } catch (Exception e) {
      throw new IllegalArgumentException(e);
    }
  }
}
