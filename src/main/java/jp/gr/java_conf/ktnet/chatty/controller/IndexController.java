package jp.gr.java_conf.ktnet.chatty.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * トップページ用のコントローラーです.
 * @author tanabe
 *
 */
@Controller
public class IndexController {

  /**
   * トップページのテンプレート名を指定します.
   * @return テンプレート名.
   */
  @RequestMapping("/")
  public String getTopPageName() {
    return "index";
  }
}
