package jp.gr.java_conf.ktnet.chatty.controller;

import jp.gr.java_conf.ktnet.chatty.entity.Cursor;
import lombok.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * チャット時のWebSocket接続処理を行うクラスです.
 * @author tanabe
 *
 */
@Component
public class ChatWsHandler extends TextWebSocketHandler {

  /**
   * 接続中のセッション情報.
   */
  private Map<String, WebSocketSession> sessions = new ConcurrentHashMap<>();
  
  /**
   * 接続中ユーザーのカーソル情報.
   */
  private Map<String, Cursor> cursors = new ConcurrentHashMap<>();
  
  @Override
  public void afterConnectionEstablished(WebSocketSession session) throws Exception {
    // 接続が確立されたときに呼ばれる.
    System.out.println("[接続]");
    sessions.put(makeKey(session), session);
    System.out.println("[接続数]" + sessions.size());
  }
  
  @Override
  public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
    // 接続が切断されたときに呼ばれる.
    System.out.println("[切断]" + status);
    String key = makeKey(session);
    sessions.remove(key);
    cursors.remove(key);
    System.out.println("[接続数]" + sessions.size());
  }

  @Override
  public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
    // メッセージを受け取ったときに呼ばれる.
    System.out.println("[受信]" + message);
    
    String key = makeKey(session);
    Cursor cursor = parseMessage(message);
    if (cursor != null) {
      if (cursors.containsKey(key)) {
        cursors.remove(key);
      }
      cursors.put(key, cursor);
    }
    
    sendBroadcastMessage(makeMessage(cursors));
  }
  
  @Override
  public void handleTransportError(WebSocketSession session,
          Throwable exception) throws Exception {
    System.out.println("[エラー]" + exception);
  }
  
  /**
   * セッション保持用IDを生成します.
   * @param session セッション.
   * @return ID.
   */
  private String makeKey(@NonNull WebSocketSession session) {
    return session.getId();
  }
  
  /**
   * メッセージを解析してオブジェクトに格納します.
   * @param message メッセージ.
   * @return 解析結果. 解析できない場合はnull.
   */
  private Cursor parseMessage(@NonNull TextMessage message) {
    try {
      String messageStr = new String(message.asBytes());
      return Cursor.makeObject(messageStr);
    } catch (Exception e) {
      System.out.println("[解析失敗]" + message);
      System.out.println(e);
      return null;
    }
  }
  
  /**
   * カーソル情報をクライアントに送るメッセージを作成します.
   * @param cursors カーソル情報.
   * @return メッセージ.
   */
  private String makeMessage(@NonNull Map<String, Cursor> cursors) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      return mapper.writeValueAsString(cursors.values());
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  
  /**
   * 接続中の全クライアントにメッセージを送付します.
   * @param message メッセージ.
   */
  private void sendBroadcastMessage(String message) {
    if (message == null || message.isEmpty()) {
      return;
    }
    TextMessage textMessage = new TextMessage(message);
    for (String key : sessions.keySet()) {
      WebSocketSession session = sessions.get(key);
      try {
        session.sendMessage(textMessage);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
