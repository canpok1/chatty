package jp.gr.java_conf.ktnet.chatty.entity;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.samePropertyValuesAs;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

@RunWith(Enclosed.class)
public class CursorTest {

  public static class makeObjectの引数が {
    
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    @Test
    public void nullだと例外発生() throws Exception {
      expectedException.expect(NullPointerException.class);
      Cursor.makeObject(null);
    }
    
    @Test
    public void 空の文字列だと例外発生() throws Exception {
      expectedException.expect(IllegalArgumentException.class);
      Cursor.makeObject("");
    }
    
    @Test
    public void 空のJSON文字列だと変換可能() throws Exception {
      String json = "{ }";
      Cursor expected = new Cursor(null, 0, 0);
      Assert.assertThat(Cursor.makeObject(json), is(samePropertyValuesAs(expected)));
    }
    
    @Test
    public void 属性名がクオートなしJSON文字列だと例外発生() throws Exception {
      expectedException.expect(IllegalArgumentException.class);
      Cursor.makeObject("{ name: 'name', x: 10, y: 11 }");
    }
    
    @Test
    public void 全要素が含まれるJSON文字列だと変換可能() throws Exception {
      String json = "{ 'name': 'name', 'x': '5', \"y\": -4 }";
      Cursor expected = new Cursor("name", 5, -4);
      Assert.assertThat(Cursor.makeObject(json), is(samePropertyValuesAs(expected)));
    }
    
    @Test
    public void 余計な要素が含まれるJSON文字列だと変換可能() throws Exception {
      String json = "{ 'name': 'ユーザー', 'x': '5', 'y': \"-4\", 'z': 0 }";
      Cursor expected = new Cursor("ユーザー", 5, -4);
      Assert.assertThat(Cursor.makeObject(json), is(samePropertyValuesAs(expected)));
    }
  }
  
  public static class makeJsonの引数が {
    
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    @Test
    public void nullを渡すと例外発生() throws Exception {
      expectedException.expect(NullPointerException.class);
      Cursor.makeJson(null);
    }
    
    @Test
    public void 非nullだと変換可能() throws Exception {
      String expected = "{\"name\":\"ユーザー\",\"x\":10,\"y\":-4}";
      Cursor object = new Cursor("ユーザー", 10, -4);
      
      Assert.assertThat(Cursor.makeJson(object), is(expected));
    }
  }
  
}
